﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFcodeFirst
{
  class Program
  {
    static void Main(string[] args)
    {
      using (var db = new SportContext())
      {
        //prompt to create new instance of ball
        Console.Write("\t**New ball type**\nEnter name: ");
        var ballName = Console.ReadLine();

        Console.Write("Enter weight, in pounds: ");
        var ballWeight = Convert.ToDecimal(Console.ReadLine());

        Console.Write("Enter material as a literal (Plastic, Rubber, Inflatable, Stuffed, Other): ");
        var ballMaterial = (Material)Enum.Parse(typeof(Material), Console.ReadLine());
        //constructor
        var ball = new Ball
        {
          BallName = ballName,
          LbWeight = ballWeight,
          Make = ballMaterial
        };

        //insert it
        db.Balls.Add(ball);
        db.SaveChanges();

        //query 4 ballz
        var getAllBalls =
          from b in db.Balls
          orderby b.BallName
          select b;
        
        
        //print em out boys
        Console.WriteLine("Entries in Ball table:\n");
        foreach (var b in getAllBalls)
        {
          Console.WriteLine("{0} ||| {1:F2} ||| {2}", b.BallName, b.LbWeight, b.Make);
        }

        Console.WriteLine("\nEnter c to clear Ball table, otherwise, enter some other key.");
        
        if(Console.Read() == 'c')
        {
        //clear em bois
        db.Database.ExecuteSqlCommand("DELETE FROM Balls");
        }


      }
      //end
      Console.WriteLine("Press any key to close.");
      Console.Read();
    }
  }


  public class Sport
  {
    [Key, ForeignKey("Ball")]
    public int SportId { get; set; }
    public String SportName { get; set; }
    public int NumberPlayers { get; set; }
    public decimal FieldWidth { get; set; }
    public decimal FieldLength { get; set; }


    public virtual Ball Ball { get; set; }
  }

  public enum Material { Plastic, Rubber, Inflatable, Stuffed, Other }
  public class Ball
  {
    public int BallId { get; set; }
    public String BallName { get; set; }
    public decimal LbWeight { get; set; }
    public Material Make { get; set; }


    public int SportId { get; set; }
    public virtual Sport Sport { get; set; }
  }

  public class Player
  {
    public int PlayerId { get; set; }
    public String PlayerName { get; set; }


    public int TeamId { get; set; }
    public virtual Team Team { get; set; }
  }

  public class Team
  {
    public int TeamId { get; set; }
    public String TeamName { get; set; }
   
    
    public Sport Sport { get; set; }
    public List<Player> Roster { get; set; }
  }

  public class SportContext : DbContext
  {
    public DbSet<Sport> Sports { get; set; }
    public DbSet<Ball> Balls { get; set; }
    public DbSet<Player> Players { get; set; }
    public DbSet<Team> Teams { get; set; }
  }

}
